<?php
/**
 * Plugin Name: Ozone Sites  
 * Plugin URI: http://ozonegroup.co
 * Description: This plugin add special behavior to Ozone Web Services.
 * Version: 1.0
 * Author: Ozone Group
 * Author URI: http://ozonegroup.co
 * License: GPL2
 */

//DEBUG LOGIC
if ( ! function_exists('write_log')) {
   function write_log ( $log )  {
      if ( is_array( $log ) || is_object( $log ) ) {
         error_log( print_r( $log, true ) );
      } else {
         error_log( $log );
      }
   }
}


//WOOCOMMERCE CHECKOUT FIELDS OPTIMIZATION
function custom_remove_woo_checkout_fields( $fields ) {

    // remove billing fields
   // unset($fields['billing']['billing_first_name']);
   // unset($fields['billing']['billing_last_name']);
    //unset($fields['billing']['billing_company']);
   // unset($fields['billing']['billing_address_1']);
  //  unset($fields['billing']['billing_address_2']);
   // unset($fields['billing']['billing_city']);
    unset($fields['billing']['billing_postcode']);
  //  unset($fields['billing']['billing_country']);
   // unset($fields['billing']['billing_state']);
    //unset($fields['billing']['billing_phone']);
  //  unset($fields['billing']['billing_email']);
   
    // remove shipping fields 
    unset($fields['shipping']['shipping_first_name']);    
    unset($fields['shipping']['shipping_last_name']);  
    unset($fields['shipping']['shipping_company']);
    unset($fields['shipping']['shipping_address_1']);
    unset($fields['shipping']['shipping_address_2']);
    unset($fields['shipping']['shipping_city']);
    unset($fields['shipping']['shipping_postcode']);
    unset($fields['shipping']['shipping_country']);
    unset($fields['shipping']['shipping_state']);
    
    // remove order comment fields
    unset($fields['order']['order_comments']);
    
    return $fields;
}
add_filter( 'woocommerce_checkout_fields' , 'custom_remove_woo_checkout_fields' );
add_filter( 'woocommerce_cart_needs_shipping_address', '__return_false');


add_filter('woocommerce_billing_fields', 'custom_woocommerce_billing_fields');

function custom_woocommerce_billing_fields($fields)
{

    $fields['tax_id'] = array(
        'label' => __('Cédula o NIT (Número de identicación tributaria)', 'woocommerce'), // Add custom field label
        'required' => true, // if field is required or not
        'clear' => false, // add clear or not
        'type' => 'text', // add field type
        'class' => array('my-css')    // add class name
    );

    return $fields;
}


add_action( 'woocommerce_checkout_update_order_meta', 'saving_checkout_tax_id');
function saving_checkout_tax_id( $order_id ) {

    $tax_id = $_POST['tax_id'];
    if ( ! empty( $tax_id ) )
        update_post_meta( $order_id, 'tax_id', sanitize_text_field( $tax_id ) );

}

